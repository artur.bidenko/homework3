/* Теоретичне питання
1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Наприме объект user имеет свойства name, age, то newUser их унаследует, если у него нет своих таких свойств явно указаных

2) Для чого потрібно викликати super() у конструкторі класу-нащадка?
super как раз нужен для того чтобы вызвать родительский конструктор и унаследовать его свойства
*/

class Employee {
	constructor({name, age, salary}) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}

	get name(){
		return this._name;
	}

	set name(newName){
		this._name = newName;
	}

	get age(){
		return this._age;
	}

	set age(newAge){
		this._age = newAge;
	}

	get salary(){
		return this._salary;
	}
	
	set salary(newSalary){
		this._salary = newSalary;
	}
}

class Programmer extends Employee {
	constructor({name, age, salary, lang}){
		super({name, age, salary});
		this.lang = lang;
	}

	get lang(){
		return this._lang;
	}
	
	set lang(newLang){
		this._lang = newLang;
	}

	get salary(){
		return this._salary * 3;
	} 
}

const programmerJS = new Programmer({name: 'Arthur', age: 32, salary: 20000, lang: 'JavaScript'});
const programmerPython = new Programmer({name: 'Dima', age: 38, salary: 60000, lang: 'Python'});
const programmerJavaRuby = new Programmer({name: 'Oleh', age: 26, salary: 70000, lang: ['Java', 'Ruby on rails']});

// console.log(programmerJS)
// console.log(programmerPython)
// console.log(programmerJavaRuby)

function renderUserData(userData) {
	const wrap = document.querySelector('.wrap');
	const userDataContainer = document.createElement('div');
	userDataContainer.classList.add('userData');
	wrap.style.display = 'flex';
	wrap.style.justifyContent = 'center';
	wrap.style.alignItems = 'center';
	wrap.style.height = '100vh';
	wrap.style.gap = '50px';
	userDataContainer.style.fontSize = '18px';
	userDataContainer.style.padding = '10px';
	userDataContainer.style.border = '2px solid #000';
	userDataContainer.innerHTML = `
		<div>Name: ${userData.name}</div>
		<div>Age: ${userData.age}</div>
		<div>Salary: ${userData.salary}</div>
		<div>Language: ${Array.isArray(userData.lang) ? userData.lang.join(', ') : userData.lang}</div>
	`;
	wrap.appendChild(userDataContainer);
}

renderUserData(programmerJS)
renderUserData(programmerPython)
renderUserData(programmerJavaRuby)