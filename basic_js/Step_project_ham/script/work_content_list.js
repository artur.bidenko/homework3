const WOKR_CONTENT_LIST = [
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design1.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design2.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design3.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design4.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design5.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design6.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design7.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design8.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design9.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design10.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design11.jpg',
    },
    {
        category: 'graphic-design',
        imageSrc: './img/graphic design/graphic-design12.jpg',
    },
    {
        category: 'web-design',
        imageSrc: './img/web design/web-design1.jpg',
    },
    {
        category: 'web-design',
        imageSrc: './img/web design/web-design2.jpg',
    },
    {
        category: 'web-design',
        imageSrc: './img/web design/web-design3.jpg',
    },
    {
        category: 'web-design',
        imageSrc: './img/web design/web-design4.jpg',
    },
    {
        category: 'web-design',
        imageSrc: './img/web design/web-design5.jpg',
    },
    {
        category: 'web-design',
        imageSrc: './img/web design/web-design6.jpg',
    },
    {
        category: 'web-design',
        imageSrc: './img/web design/web-design7.jpg',
    },
    {
        category: 'landing-pages',
        imageSrc: './img/landing page/landing-page1.jpg',
    },
    {
        category: 'landing-pages',
        imageSrc: './img/landing page/landing-page2.jpg',
    },
    {
        category: 'landing-pages',
        imageSrc: './img/landing page/landing-page3.jpg',
    },
    {
        category: 'landing-pages',
        imageSrc: './img/landing page/landing-page4.jpg',
    },
    {
        category: 'landing-pages',
        imageSrc: './img/landing page/landing-page5.jpg',
    },
    {
        category: 'landing-pages',
        imageSrc: './img/landing page/landing-page6.jpg',
    },
    {
        category: 'landing-pages',
        imageSrc: './img/landing page/landing-page7.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress1.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress2.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress3.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress4.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress5.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress6.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress7.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress8.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress9.jpg',
    },
    {
        category: 'wordpress',
        imageSrc: './img/wordpress/wordpress10.jpg',
    },
]