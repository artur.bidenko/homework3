/*Теоретичні питання

1. Экранирование это обратный слеш, который позволяет вывести следующий за ним символ например "Ім\'я" в данном случае строка будет с апострофом

2. function declaration, function expression, стрелочные функции. 

3. Хостинг это возмоность получить доступ к функции и переменным до того как они были созданы.

*/

const userName = prompt('Enter your name'); 
const userSurname = prompt('Enter your surname');
const userBirthday = prompt('Enter your date of birth, dd.mm.yyyy')

function createNewUser(name, last, age) {
	const newUser = {
		_firstName: name,
		_lastName: last,
		birthday: age,
		getAge() {
			const [day, month, year] = this.birthday.split(".");
			console.log([day, month, year])
			const today = new Date();
			const age = today.getFullYear() - year - ((today.getMonth() + 1 < month || (today.getMonth() + 1 == month && today.getDate() < day)) ? 1 : 0);
			return age;
		},
		getPassword() {
			const year = this.birthday.slice(6);
			return `${this.firstName.slice(0, 1).toUpperCase()}${this.lastName.toLowerCase()}${year}`;
		},
		getLogin() {
			return `${this.firstName.slice(0, 1).toLowerCase()}${this.lastName.toLowerCase()}`;
		},
		set firstName(newName) {
			this._firstName = newName;
		},
		get firstName() {
			return this._firstName;
		},

		set lastName(newSurname) {
			this._lastName = newSurname;
		},
		get lastName() {
			return this._lastName;
		},

		setFirstName(newName) {
			this.firstName = newName;
		},
		setLastName(newSurname) {
			this.lastName = newSurname;
		}
	}
newUser.setFirstName(userName);
newUser.setLastName(userSurname);
return newUser;
}
let user = createNewUser(userName, userSurname, userBirthday)
console.log(user)
console.log(user.getLogin()) 
console.log(user.getAge()) 
console.log(user.getPassword()) 

