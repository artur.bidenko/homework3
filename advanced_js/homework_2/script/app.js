/* Теоретичне питання
1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Наприме объект user имеет свойства name, age, то newUser их унаследует, если у него нет своих таких свойств явно указаных

2) Для чого потрібно викликати super() у конструкторі класу-нащадка?
super как раз нужен для того чтобы вызвать родительский конструктор и унаследовать его свойства
*/

const books = [
	{ 
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70 
	}, 
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	}, 
	{ 
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	}, 
	{ 
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	}, 
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

function renderBooksList() {
	const rootElement = document.getElementById('root');
	const ulElement = document.createElement('ul');
	const body = document.querySelector('body');

	books.forEach(book => {
		try {
			if(checkIsValidBook(book)){
				const liElement = document.createElement('li');
				liElement.textContent = getPropertiesBook(book);
				ulElement.appendChild(liElement);
				Object.assign(body.style, {
					backgroundColor: '#1f1f24',
					margin: '0px',
					padding: '0px',
					overflow: 'hidden',
				});
				Object.assign(ulElement.style, {
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'center',
					alignItems: 'center',
					height: '100vh',
				});
				Object.assign(liElement.style, {
					color: '#fff',
					fontSize: '24px',
					lineHeight: '44px',
					listStyleType: 'none',
				});
			}
		}
		catch (error){
			console.error(error);
		}
	});

	rootElement.appendChild(ulElement);
}

function checkIsValidBook(book) {
	if(!book.hasOwnProperty('author')) {
		throw new Error (`Помилка: Об'єкт ${JSON.stringify(book)} не містить властивості 'author'.`);
	}

	if(!book.hasOwnProperty('name')) {
		throw new Error (`Помилка: Об'єкт ${JSON.stringify(book)} не містить властивості 'name'.`);
	}

	if(!book.hasOwnProperty('price')) {
		throw new Error (`Помилка: Об'єкт ${JSON.stringify(book)} не містить властивості 'price'.`);
	}

	return true;
}

function getPropertiesBook(book) {
	try {
		if(book.author && book.name && book.price) {
			return `${book.author} - ${book.name}: (${book.price} грн)`;
		}
	}
	catch(error) {
		console.error(error);
	}
}

renderBooksList(); 