const student = {
	name: "",
	lastName: "",
	grades: []
};

student.name = prompt("Enter student's name:");
student.lastName = prompt("Enter student's last name:");

let gradeInput = true;
while (gradeInput) {
	const subjectName = prompt("Enter subject name:");
	if (subjectName === null) {
		gradeInput = false;
		continue;
	}
	const grade = parseFloat(prompt(`Enter ${subjectName} grade:`));
	if (!isNaN(grade)) {
		student.grades.push({
		subject: subjectName,
		grade: grade
		});
	}
}

let badGrades = 0;
for (const grade of student.grades) {
	if (grade.grade < 4) {
    badGrades++;
	}
}
console.log(`bad grades ${badGrades}`)

let totalGrade = 0;
for (const grade of student.grades) {
	totalGrade += grade.grade;
}
const gpa = totalGrade / student.grades.length;
console.log(`average grade: ${gpa}`)
console.log(`total grade: ${totalGrade}`)

if (badGrades === 0) {
	console.log('Студент переведено на наступний курс');
}
if (gpa > 7) {
	console.log('Студенту призначено стипендію');
}