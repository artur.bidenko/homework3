import { Formik, Form,  } from "formik";
import PropTypes from 'prop-types';
import Button from "../../Button";
import Input from "../Input";
import {BtnWrapper, FlexWrapper, FormHeader, FormWrapper, Price, StyledFieldset} from './styledCartForm'
import { validationSchema } from "./validations";


const CartForm = ({amount, totalPrice, onSubmit}) => {
    return (
        <Formik
            initialValues = {
                {
                    name: "John",
                    surname: "Doe",
                    adress: "42 Vasylkivska street Kyiv, 01044",
                    age: '35',
                    phone: ""
                }
            }
            onSubmit={(values) => onSubmit(values)}
            validationSchema={validationSchema}
        >
            {({errors, touched}) => (
            <FormWrapper>
                <Form>
                    <StyledFieldset >
                    <FormHeader>Please fill out the purchase form</FormHeader>
                        <FlexWrapper>
                            <Input name="name" label='Name' placeholder="name"
                                    errors={errors} error={errors.name && touched.name}/>

                            <Input name="surname" label='Surname' placeholder="surname"
                                    error={errors.surname && touched.surname}/>

                            <Input name="age" label='Age' placeholder="age" type='number'
                                    error={errors.age && touched.age}/>

                            <Input name="phone" label='Phone' placeholder="phone"
                                    error={errors.phone && touched.phone} phone={true}/>
                        </FlexWrapper>
                        <FlexWrapper>
                            <Input name="adress" label='Delivery address' placeholder="delivery address"
                                    error={errors.adress && touched.adress} width='100%'/>
                        </FlexWrapper>
                        <Price>You have {amount} item(s) with total value {totalPrice}$</Price>
                    </StyledFieldset>
                    
                    <BtnWrapper>
                        <Button type="submit" backgroundColor='green'>Checkout</Button>
                    </BtnWrapper>

                </Form>
            </FormWrapper>
            )}
        </Formik>
    )
}

CartForm.propTypes = {
    amount: PropTypes.number,
    totalPrice: PropTypes.number,
    onSubmit: PropTypes.func
}

export default CartForm