window.onload = function () {

const tabs = document.querySelector('.tabs')
const tabsContent = document.querySelector('.tabs-content')
const activeTab = document.querySelector('.tabs-content .active')

const showContentActiveTab = () => {
    tabsContent.querySelectorAll('.tabs-content-container').forEach(content => {
        content.style.display = 'none'
    })
    activeTab.style.display = 'flex'
}

const showTabWithContent = (event) => {
    const clickedTab = event.target.closest('.tabs-title')
    if (!clickedTab) {
		return
	}
    const index = Array.from(tabs.children).indexOf(clickedTab)

    tabs.querySelectorAll('.tabs-title').forEach(tab => {
        tab.classList.remove('active')
    })
    clickedTab.classList.add('active')

    tabsContent.querySelectorAll('.tabs-content-container').forEach(content => {
        content.style.display = 'none'
    })
    tabsContent.querySelectorAll('.tabs-content-container')[index].style.display = 'flex'
}

tabs.addEventListener('click', showTabWithContent)

showContentActiveTab()






// section "Our Amazing Work"

    function getItem (imageSrc) {
     return `
            <div class="container-work">
                <img class="content-work" src="${imageSrc}" alt="Graphic Design">
            </div>
        `
    }

    const loadMore = document.querySelector('.btn-load-more');

let currentCategory = '';

let isLoadMore = true;

// контент
    const containerContent = document.querySelector('.container-content')
    let htmlWorkContentList = '';
    const currentItemList = [...WOKR_CONTENT_LIST];
    currentItemList.length = 12;
    let skip = currentItemList.length;
    currentItemList.forEach(({ imageSrc}, index) => {
        htmlWorkContentList += getItem(imageSrc);
    });

containerContent.innerHTML = htmlWorkContentList;

function getContentListByCategory () {
    return currentCategory
        ? WOKR_CONTENT_LIST.filter((item) => item.category === currentCategory)
        : [...WOKR_CONTENT_LIST];
}

function loadMoreItems () {
    skip += 12;
    const currentContentList = getContentListByCategory();
    isLoadMore = skip <= currentContentList.length
    currentContentList.length = skip;
    let currentHtmlContent = '';
    currentContentList.forEach(({ imageSrc}, index) => {
        currentHtmlContent += getItem(imageSrc);
    });
    containerContent.innerHTML = currentHtmlContent;
}

function onChangeCategory (e) {
    currentCategory = e.target.id === 'all' ? '' : e.target.id;
    skip = 12;
    const currentContentList = getContentListByCategory();
    isLoadMore = skip <= currentContentList.length
    let currentHtmlContent = '';
    currentContentList.forEach(({ imageSrc}, index) => {
        currentHtmlContent += getItem(imageSrc);
    });
    containerContent.innerHTML = currentHtmlContent;
}




// кнопка show more

// const loadMore = document.querySelector('.btn-load-more')
// let currentItems = 12;

// loadMore.addEventListener('click', (elem) => {
//     const elementList = [...document.querySelectorAll('.container-content div')];
//     elem.target.classList.add('show-loader')

//     for (let i = currentItems; i < currentItems + 12; i++) {
//         setTimeout( function() {
//             elem.target.classList.remove('show-loader')
//             if(elementList[i]) {
//                 elementList[i].style.display = 'flex';
//             }
//         }, 3000)
//     }
//     currentItems += 12

//     if(currentItems >= elementList.length) {
//         event.target.classList.add('loaded')
//     }
// })


// еще один вариант

const contentWorkLength = document.querySelectorAll('.content-work').length
console.log(contentWorkLength)
let items = 12;

loadMore.addEventListener('click', () => {
    loadMoreItems();
    // items += 12;
    //
    // const array = Array.from(containerContent.children)
    // const visItems = array.slice(0, items)
    // console.log(visItems)
    //
    // visItems.forEach(elem  => {
    //     elem.classList.add('visible')
    // })
    //
    // if(visItems.length === contentWorkLength) {
    //     loadMore.style.display = 'none'
    // }
})





// фильтр

// const tabsContentWork = document.querySelector('.tabs-content-work');
// const contentWork = document.querySelectorAll('.content-work');
// const tabsTitleWork = document.querySelectorAll('.tabs-title-work')

// function filter(){
// tabsContentWork.addEventListener('click', event => {
//     const targetId = event.target.dataset.id
//     const target = event.target

//     tabsTitleWork.forEach(tabsValue => tabsValue.classList.remove('active'))
//     target.classList.add('active')

//     switch(targetId) {
//         case 'all':
//             getItems('content-work')
//             break;
//         case 'graphic-design':
//             getItems(targetId)
//             break;
//         case 'web-design':
//             getItems(targetId)
//             break;
//         case 'landing-pages':
//             getItems(targetId)
//             break;
//         case 'wordpress':
//             getItems(targetId)
//             break;
//     }
//     })
// }
// filter()


// function getItems(categoryName) {
//     contentWork.forEach(item => {
//         if (item.classList.contains(categoryName)) {
//             item.style.display = 'block'
//         } else {
//             item.style.display = 'none'
//         }
//     })
// }



// еще один вариант фильтра

const filterContent = document.querySelectorAll('.content-work');
console.log(filterContent)
const tabsContentWork = document.querySelector('.tabs-content-work');
console.log(tabsContentWork)

    const categoryAllBtn = document.getElementById('all');
    const categoryGraphicBtn = document.getElementById('graphic-design');
    const categoryWebBtn = document.getElementById('web-design');
    const categoryLandingBtn = document.getElementById('landing-pages');
    const categoryWordpressBtn = document.getElementById('wordpress');

    tabsContentWork.querySelectorAll('.tabs-title-work').forEach(tab => {
        tab.addEventListener('click', onChangeCategory)
      // tab.classList.remove('active')
    })


// tabsContentWork.addEventListener('click', (event)=> {
//     const clickedTabs = event.target.closest('.tabs-title-work')
//     if (!clickedTabs)
//         return false;
//
//     tabsContentWork.querySelectorAll('.tabs-title-work').forEach(tab => {
//         tab.classList.remove('active')
//     })
//     clickedTabs.classList.add('active')
//
//     const filterClass = event.target.dataset['id'];
//
//     filterContent.forEach( (el)=> {
//         const elem = el.classList;
//
//         elem.remove('hide');
//         if (!elem.contains(filterClass) && filterClass !== 'all') {
//             elem.add('hide');
//         }
//     });
// });













//slider - section client-feedback

const slides = document.querySelectorAll('.slider')
const clientFeedbackContainer = document.querySelectorAll('.client-feedback-container')
const clientFeedbackSlider = document.querySelector('.client-feedback-slider')
const prevArrow = document.querySelector('.prev-arrow')
const nextArrow = document.querySelector('.next-arrow')

let currentSlide = 0
let currentActive = 0

    function playSlide(slide) {

        for (let i = 0; i < clientFeedbackContainer.length; i++) {
            slides[i].classList.remove('active');
            clientFeedbackContainer[i].classList.remove('active');
        }

        if (slide < 0) {
            slide = currentSlide = slides.length - 1;
        }

        if (slide > slides.length - 1) {
            slide = currentSlide = 0;
        }

        slides[slide].classList.add('active');
        clientFeedbackContainer[slide].classList.add('active');

        currentActive = currentSlide;
    }

    prevArrow.addEventListener('click', () => {
        playSlide(currentSlide -= 1);
    });

    nextArrow.addEventListener('click', () => {
        playSlide(currentSlide += 1);
    });

playSlide(currentSlide);


const showSlideWithContent = (event) => {
    const clickedslider = event.target.closest('.slider')
    if (!clickedslider) {
		return
	}
    const index = Array.from(clientFeedbackSlider.children).indexOf(clickedslider)

    clientFeedbackSlider.querySelectorAll('.slider').forEach(slider => {
        slider.classList.remove('active')
    })
    clickedslider.classList.add('active')

    document.querySelectorAll('.client-feedback-container').forEach(content => {
        content.classList.remove('active')
    })
    document.querySelectorAll('.client-feedback-container')[index].classList.add('active')
}

clientFeedbackSlider.addEventListener('click', showSlideWithContent)

}
