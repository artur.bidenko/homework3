import React from "react";
import PropTypes from 'prop-types';
import ItemsList from "../../components/ItemsList";
import PageWrapper from "../../components/PageWrapper/PageWrapper";

const MainPage = (props) => {

    const {itemsList, openModal, addToFavorite, textBtn, widthBtn} = props;

    return (
        <PageWrapper>
            <h2>Look at our goods</h2>
            <ItemsList itemsList={itemsList} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn} widthBtn={widthBtn}/>
        </PageWrapper>
    )
}

MainPage.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string
}

export default MainPage;