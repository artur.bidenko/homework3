/*
	1. Как я понял это древовидная структура HTML документа в которой все теги являются объектами и копию которого создает js и благодоря этому можно взаимодействовать с HTML документом, то-есть добовлять динамику на UI 

	2. innerText вернет нам все что будет вложено в текстовом формате включая теги и другой код, а innerHTML создаст тег и вложит в него текст например 

	3. Обратиться к элементу можно разными способами, взависимости что элемент содержит например 
	По id - document.getElementById;
	По классу — document.getElementsByClassName;
	По тэгу — document.getElementsByTagName;
	По CSS селектору (один элемент) — document.querySelector;
	По CSS селектору (много элементов) — document.querySelectorAll.
	Мне больше наривится обращаться к жлементу по селектору, так как мне кажеться он блее гибкий, с помощью селектора я могу обратиться к объекту по классу, по id, по тегу 
*/



const paragraphs = document.querySelectorAll('p')

for (let item of paragraphs) {
	item.style.backgroundColor = '#ff0000';
}


const optionsList = document.querySelector('#optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);
const childNodes = optionsList.childNodes;

for (let i = 0; i < childNodes.length; i++) {
	console.log(`${childNodes[i].nodeName} - ${childNodes[i].nodeType}`);
}


const testParagraph = document.querySelector('#testParagraph')
testParagraph.innerHTML = 'This is a paragraph'
console.log(testParagraph)


const mainHeader = document.querySelectorAll('.main-header > div');
mainHeader.forEach((el) => {
	console.log(el);
	el.classList.add("nav-item");
});


const sectionTitle = document.querySelectorAll('.section-title')
console.log(sectionTitle)
sectionTitle.forEach((el) => {
	el.classList.remove('section-title')
})
console.log(sectionTitle)