import PropTypes from 'prop-types';
import {ReactComponent as LeftArrow} from "./images/left_arrow.svg";
import { BackSpan } from './StyledBackBtn';

const BackBtn = ({goBack}) => {
    return (
        <BackSpan onClick={goBack}>
            <LeftArrow/>
        </BackSpan>
    )
}

BackBtn.propTypes = {
    goBack: PropTypes.func
}

export default BackBtn