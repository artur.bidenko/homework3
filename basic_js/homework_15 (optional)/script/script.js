/*Теоретичні питання
	Рекурсия это функия, которая вызывает сама себя, если у нас не сложный алгоритм, который выполняется в несколько шагов его можно описать рекурсией, поидее код может быть короче чем писать через цикл например.
*/

let number = prompt('Enter your number','')

while (!Number(number)) {
	if(number === null) {
		break;
	}
	number = prompt('Enter your number', number);
}

const factorial = (n, result) => { 
	result = result || 1; 
	if(!n){ 
		return result; 
	} else{ 
		return factorial(n-1, result*n); 
	} 
}
console.log(factorial(number))