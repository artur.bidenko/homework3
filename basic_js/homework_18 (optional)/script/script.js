function cloneObject(obj) {
	let clone = {};
	console.log(clone)
	for (let key in obj) {
		if (typeof obj[key] === "object" && obj[key] !== null) {
		clone[key] = cloneObject(obj[key]);
		} else if (Array.isArray(obj[key])) {
		clone[key] = obj[key].map((item) =>
			typeof item === "object" && item !== null ? cloneObject(item) : item
		);
		} else {
		clone[key] = obj[key];
		}
	}

	return clone;
}	

console.log(cloneObject(users))