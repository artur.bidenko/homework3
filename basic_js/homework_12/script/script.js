/*
	1. Потому, что данные можно вводить не только с помощью клавиатуры, а например копировать, это не надежно
*/


const buttons = document.querySelectorAll(".btn");

const keydown = () => {
	document.addEventListener('keydown', (event) => {
	let invalidInput = false;

	buttons.forEach((btn) => {
		if (btn.classList.contains("active")) {
		btn.classList.remove("active");
		}
		if (`Key${btn.dataset.key}` === event.code || 
		(event.key === "Enter" && btn.dataset.key === "Enter")
		) {
		invalidInput = true;
		btn.classList.add("active");
		}
	});

	if (!invalidInput) {
		console.error('Opps')
	}
})
}
keydown()
