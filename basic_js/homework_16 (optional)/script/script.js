
let n = Number(prompt('Enter the number of the Fibonacci number'));
let firstNum = Number(prompt('Enter the first number'));
let secondNum = Number(prompt('Enter the second number'));

function fib(firstNum, secondNum, n) {
	if (n < 0) {
		return fib(secondNum - firstNum, firstNum, - n);
	}
	
	let firstTwoNumbers = [firstNum, secondNum];
	
	for (let i = 2; i <= n; i++) {
		firstTwoNumbers[i] = firstTwoNumbers[i - 1] + firstTwoNumbers[i - 2];
	}
	return firstTwoNumbers[n];
}
let result = fib(firstNum, secondNum, n);

console.log(`ordinal number ${n} the sum of your Fibanacci number ${result}`);