/*Теоретичні питання

	1. Функция которая вызывается внутри объекта называется методом

	2. Значения могут иметь любой тип данных

	3. Объект в себе хранит ссылку на значения 
*/

const userName = prompt('Enter your name');
const userSurname = prompt('Enter your surname');
function createNewUser() {
	const newUser = {
		_firstName: 'Arthur',
		_lastName: 'Bidenko',
		getLogin() {
			return `${this.firstName.slice(0, 1).toLowerCase()}${this.lastName.toLowerCase()}`
		},

		set firstName(newName) {
			this._firstName = newName;
		},
		get firstName() {
			return this._firstName;
		},

		set lastName(newSurname) {
			this._lastName = newSurname;
		},
		get lastName() {
			return this._lastName;
		},

		setFirstName(newName) {
			this.firstName = newName;
		},
		setLastName(newSurname) {
			this.lastName = newSurname;
		}
	}

newUser.setFirstName(userName);
newUser.setLastName(userSurname);
// console.log(newUser.firstName);
// newName.firstName = '23432434';
// console.log(newUser.firstName)
console.log(newUser);

}
createNewUser().getLogin();