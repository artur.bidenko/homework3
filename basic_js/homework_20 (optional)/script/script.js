


// const filterCollection = (arr, StringWithKeywords, boolean) => {
	
// 	arr.filter((value, index, array) => {

// 	})
// }

// filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description')




// function filterCollection(collection, keywords, matchAll, ...fields) {
// 	const keywordArr = keywords.toLowerCase().split(' ');
// 	return collection.filter((item) => {
// 	const foundFields = fields.filter((field) => field.includes('...'));
// 	const matchFields = fields.filter((field) => !foundFields.includes(field));
// 	const fieldValues = [];
// 	matchFields.forEach((field) => {
// 	const value = field.split('.').reduce((obj, key) => obj ? obj[key] : '', item);
// 	if (value !== undefined && value !== null) {
// 		fieldValues.push(value.toString().toLowerCase());
// 	}
// 	});
// 	foundFields.forEach((field) => {
// 	const keys = field.split('...');
// 	const value = keys.reduce((obj, key) => obj ? obj[key] : '', item);
// 	if (value !== undefined && value !== null) {
// 		if (Array.isArray(value)) {
// 		value.forEach((val) => {
// 			const nestedValue = keys.slice(1).reduce((obj, key) => obj ? obj[key] : '', val);
// 			if (nestedValue !== undefined && nestedValue !== null) {
// 			fieldValues.push(nestedValue.toString().toLowerCase());
// 			}
// 		});
// 		} else {
// 		fieldValues.push(value.toString().toLowerCase());
// 		}
// 	}
// 	});
// 	if (matchAll) {
// 		return keywordArr.every((keyword) => fieldValues.some((value) => value.includes(keyword)));
// 	} else {
// 		return keywordArr.some((keyword) => fieldValues.some((value) => value.includes(keyword)));
// 	}
// 	});
// }
// filterCollection(['en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'])






function filterCollection(collection, keywords, matchAll, ...fields) {
	const searchTerms = keywords.toLowerCase().split(' ');
	return collection.filter(obj => {
		const matches = fields.some(field => {
		const value = deepValue(obj, field);
		if (value !== undefined) {
			const stringValue = String(value).toLowerCase();
			return searchTerms.every(term => stringValue.includes(term));
		}
		return false;
		});
		return matchAll ? matches && matches.length === searchTerms.length : matches;
	});
	}

function deepValue(obj, path) {
	const parts = path.split('.');
	let value = obj;
	for (const part of parts) {
		if (value !== undefined && value !== null) {
			value = value[part];
		} else {
			break;
		}
	}
	return value;
}

const vehicles = [  { 
	id: 1, 
	name: 'Toyota Corolla', 
	description: 'Compact car',
	contentType: { 
		name: 'Vehicle' }, 
	locales: [{ 
		name: 'en_US', 
		description: 'Toyota Corolla' }, 
		{ name: 'es_ES', 
		description: 'Toyota Corolla' }] },
	{ 
	id: 2, name: 'Honda Civic', 
	description: 'Compact car', contentType: { name: 'Vehicle' }, locales: [{ name: 'en_US', description: 'Honda Civic' }, { name: 'es_ES', description: 'Honda Civic' }] },]
	// { id: 3, name: 'Toyota Camry', description: 'Mid-size car', contentType: { name: 'Vehicle' }, locales: [{ name: 'en_US', description: 'Toyota Camry' }, { name: 'es_ES', description: 'Toyota Camry' }] } ]

console.log(filterCollection([vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description']))
