/*Теоретичні питання

	1. Функция нужна для того чтобы не дублировать одинаковый код, а написать его один раз внутри функции и вызывать ее где нам нужно 

	2. При вызове функции мы передаем аргумент и он записывается в параметр функции и мы можем его использовать внутри самой функции 

	3. Оператор return останавливает выполнение кода внутри функции и возвращает нам результат, который можно записать например в переменную или он вернется в место вызова функции 
	Если return указан пустой он вернет undefined.
*/


let firstNum = prompt('Write first number');
let secondNum = prompt('Write second number','');
let operator = prompt('Enter a mathematical operator: +, -, *, /.');

function getNumber(first, second) {
	let invalidFirstNum = firstNum;
	while (!Number(firstNum) || !invalidFirstNum) {
		if(firstNum === null) {
			break;
		}
		const currentFirstNum = prompt(first, firstNum);
		if(Number(currentFirstNum)) {
			firstNum = Number(currentFirstNum);
			invalidFirstNum = Number(currentFirstNum);
		} else {
			invalidFirstNum = currentFirstNum; 
		}

	}
	let invalidSecondNum = secondNum;
	while (!Number(secondNum) || !invalidSecondNum) {
		if(secondNum === null) {
			break;
		}
		const currentSecondNum = prompt(second, secondNum);
		if(Number(currentSecondNum)) {
			secondNum = Number(currentSecondNum);
			invalidSecondNum = Number(currentSecondNum);
		} else {
			invalidSecondNum = currentSecondNum;
		}
	}
}

const getOperator = (oper) => {
	while (oper !== '+' && oper !== '-' && oper !== '*' && oper !== '/') {
			if(secondNum === null) {
				break;
			}
			operator = prompt('Enter a mathematical operator: +, -, *, /.');
		}
}

const calcValue = (firstNum, secondNum) => {

	switch (operator) {
		case '+':
			console.log(firstNum + secondNum);
			break;

		case '-':
			console.log(firstNum - secondNum);
			break;

		case '*':
			console.log(firstNum * secondNum);
			break;

		case '/':
			console.log(firstNum / secondNum);
			break;

		default:
			console.error('Erorr');
	}
}

const execute = () => {
	getNumber('Write first number', 'Write second number');
	getOperator(operator);
	calcValue(Number(firstNum), Number(secondNum));
} 	
execute()