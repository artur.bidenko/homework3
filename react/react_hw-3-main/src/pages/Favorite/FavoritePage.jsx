import PropTypes from 'prop-types';
import PageWrapper from '../../components/PageWrapper/PageWrapper';
import ItemsList from '../../components/ItemsList';
import BackBtn from '../../components/BackBtn';

const FavoritePage = (props) => {
    const {itemsList, openModal, addToFavorite, textBtn, widthBtn, goBack} = props;
    return (
        <PageWrapper>
            <h2><BackBtn goBack={goBack} />Your favorite</h2>
            <ItemsList itemsList={itemsList} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn} widthBtn={widthBtn}/>
        </PageWrapper>
    )
}

FavoritePage.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string,
    goBack: PropTypes.func
}

export default FavoritePage;