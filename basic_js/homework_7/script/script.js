/*Теоретичні питання
1. По сути делает перебор по элементам массива 

2. Oчистить масив можно через метод splice (0)
	let qwe = [1, 2, 'str', 'Number', false, 'toyota', 33];
	console.log(qwe)
	qwe.splice(0)
	console.log(qwe)

	или через length

	qwe.length = 0

3. Через метод Array.isArray(value) если это масив, вернеться true
*/

const filterBy = (arr, dataType) => arr.filter((items) => typeof items !== dataType);

const arr = ['hello', 'world', 23, '23', null]
const filterData = filterBy(arr, 'string')