/* 
    1. Разница между setTimeout() и setInterval() в  том, что setTimeout() выполняет функцию один раз, через какой-то интервал времени, а setTimeout() многократно через какой-то интервал времени

    2. Если поставить нулевую задержку на setTimeout() он отработает сразу же после кода 

    3. Пока он тикает это грузит браузер, и грузит клиент пользователя 
*/
const btnStop = document.querySelector("#btn-stop");
const btnResume = document.querySelector("#btn-start");
btnResume.setAttribute("disabled", true);
let slideIndex = 0;

function showImages() {
    const slides = document.querySelectorAll(".image-to-show");

    slides.forEach((img) => {
        img.classList.add("hidden");
    })

    slideIndex++;
    if (slideIndex > slides.length) 
    slideIndex = 1; 
    slides[slideIndex - 1].classList.remove("hidden");
}
showImages();

let interval = setInterval(showImages, 3000);

btnStop.addEventListener("click", () => {
    clearInterval(interval);
    btnResume.removeAttribute("disabled");
    btnStop.setAttribute("disabled", true);
});
btnResume.addEventListener("click", () => {
    setInterval(showImages, 3000);
    btnResume.setAttribute("disabled", true);
    btnStop.removeAttribute("disabled");
});


    
