
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// 1) Асинхронность это когда определенные задачи (части кода) могут выполняться независимо друг от друга и не блокировать основной поток выполнения кода.

async function getIp() {

	try {
		const response = await fetch('https://api.ipify.org/?format=json')
		const data = await response.json()
		const dataIp = data.ip
		// console.log(dataIp)
		getAddress(dataIp)
	}
	catch(error) {
		console.error(error)
	}
}

async function getAddress(dataIp) {

	try {
		const addressResponse = await fetch(`http://ip-api.com/json/${dataIp}`)
		const addressData = await addressResponse.json()
		// console.log(addressData)
		renderContent(addressData)
	} 
	catch(error) {
		console.error(error)
	}
	
}

function renderContent(addressData) {

	try {
		const content = document.createElement('div')
		content.classList.add('content')
		document.body.appendChild(content)
		content.innerHTML = ''
		content.innerHTML = `
			<div>${addressData.timezone}</div>
			<div>${addressData.country}</div>
			<div>${addressData.regionName}</div>
			<div>${addressData.city}</div>
			<div>${addressData.zip}</div>
		`
	}
	catch(error) {
		console.error(error)
	}
}

document.addEventListener('DOMContentLoaded', async function() {
	const btn = document.querySelector('.btn');
	btn.addEventListener('click', getIp);
});