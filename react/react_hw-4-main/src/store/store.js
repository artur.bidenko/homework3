import {configureStore} from '@reduxjs/toolkit';

import thunk from 'redux-thunk'
// import logger from 'redux-logger';

import rootReducers from './reducers';

const store = configureStore({
    reducer: rootReducers,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware(thunk)
})

export default store