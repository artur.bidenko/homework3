const passwordFields = document.querySelectorAll('.input-wrapper input[type="password"]');
const showPasswordIcons = document.querySelectorAll('.icon-password');

const showPassword = () => {
	showPasswordIcons.forEach((icon, index) => {
		icon.addEventListener('click', () => {
			const passwordField = passwordFields[index];
			const type = passwordField.getAttribute('type') === 'password' ? 'text' : 'password';
			passwordField.setAttribute('type', type);
			icon.classList.toggle('fa-eye');
			icon.classList.toggle ('fa-eye-slash');
		});
	});
}
showPassword()

const form = document.querySelector('.password-form');
const confirmButton = form.querySelector('.btn');
const password = form.querySelector('#pass');
const confirmPassword = form.querySelector('#pass-confirm');
const errorText = document.createElement('span');

const validationInputsPassword = () => {
	confirmButton.addEventListener('click', (event) => {
		event.preventDefault();
		if(password.value === '' || confirmPassword.value === '' || password.value !== confirmPassword.value) {
			errorText.textContent = 'Потрібно ввести однакові значення';
			errorText.style.color = 'red';
			errorText.style.display = 'block'
			errorText.style.marginBottom = '10px'
			confirmPassword.after(errorText);
		} else {
			errorText.remove()
			alert('You are welcome')
		}
	});
}
validationInputsPassword()