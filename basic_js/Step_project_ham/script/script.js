window.onload = function () {

//section "Our Services"

const tabs = document.querySelector('.tabs')
const tabsContent = document.querySelector('.tabs-content')
const activeTab = document.querySelector('.tabs-content .active')

const showContentActiveTab = () => {
    tabsContent.querySelectorAll('.tabs-content-container').forEach(content => {
        content.style.display = 'none'
    })
    activeTab.style.display = 'flex'
}

const showTabWithContent = (event) => {
    const clickedTab = event.target.closest('.tabs-title')
    if (!clickedTab) {
		return
	}
    const index = Array.from(tabs.children).indexOf(clickedTab)

    tabs.querySelectorAll('.tabs-title').forEach(tab => {
        tab.classList.remove('active')
    })
    clickedTab.classList.add('active')

    tabsContent.querySelectorAll('.tabs-content-container').forEach(content => {
        content.style.display = 'none'
    })
    tabsContent.querySelectorAll('.tabs-content-container')[index].style.display = 'flex'
}

tabs.addEventListener('click', showTabWithContent)

showContentActiveTab()


// section "Our Amazing Work"

function getItem (imageSrc) {
return `
        <div class="container-work">
            <img class="content-work" src="${imageSrc}" alt="Graphic Design">
        </div>
    `
}

const loadMore = document.querySelector('.btn-load-more');

let currentCategory = '';

let isLoadMore = true;

const containerContent = document.querySelector('.container-content')
let htmlWorkContentList = '';
const currentItemList = [...WOKR_CONTENT_LIST];
currentItemList.length = 12;
let DEFAULT_SKIP_ITEMS = currentItemList.length;
currentItemList.forEach(({ imageSrc}) => {
    htmlWorkContentList += getItem(imageSrc);
});

containerContent.innerHTML = htmlWorkContentList;

function getContentListByCategory () {
    return currentCategory
        ? WOKR_CONTENT_LIST.filter((item) => item.category === currentCategory)
        : [...WOKR_CONTENT_LIST];
}

function loadMoreItems () {
    DEFAULT_SKIP_ITEMS += 12;
    const currentContentList = getContentListByCategory();
    isLoadMore = DEFAULT_SKIP_ITEMS <= currentContentList.length
    currentContentList.length = DEFAULT_SKIP_ITEMS;
    let currentHtmlContent = '';
    currentContentList.forEach(({ imageSrc}) => {
        currentHtmlContent += getItem(imageSrc);
    });
    containerContent.innerHTML = currentHtmlContent;

    if(!isLoadMore) {
        loadMore.style.display = 'none'
    }
}

function onChangeCategory (e) {
    const clicked = e.target.closest('.tabs-title-work');
    currentCategory = e.target.id === 'all' ? '' : e.target.id;
    DEFAULT_SKIP_ITEMS = 12;
    const currentContentList = getContentListByCategory();
    isLoadMore = DEFAULT_SKIP_ITEMS <= currentContentList.length
    currentContentList.length = DEFAULT_SKIP_ITEMS;
    let currentHtmlContent = '';
    currentContentList.forEach(({ imageSrc}, index) => {
        currentHtmlContent += getItem(imageSrc);
    });
    containerContent.innerHTML = currentHtmlContent;

    loadMore.style.display = isLoadMore ? 'block' : 'none'

    tabsContentWork.querySelectorAll('.tabs-title-work').forEach(tab => {
        tab.classList.remove('active')
    })
    clicked.classList.add('active')
}

loadMore.addEventListener('click', loadMoreItems)

const tabsContentWork = document.querySelector('.tabs-content-work');

tabsContentWork.querySelectorAll('.tabs-title-work').forEach(tab => {
    tab.addEventListener('click', onChangeCategory)
})

//slider - section client-feedback

const slides = document.querySelectorAll('.slider')
const clientFeedbackContainer = document.querySelectorAll('.client-feedback-container')
const clientFeedbackSlider = document.querySelector('.client-feedback-slider')
const prevArrow = document.querySelector('.prev-arrow')
const nextArrow = document.querySelector('.next-arrow')

let currentSlide = 0
let currentActive = 0

    function playSlide(slide) {

        for (let i = 0; i < clientFeedbackContainer.length; i++) {
            slides[i].classList.remove('active');
            clientFeedbackContainer[i].classList.remove('active');
        }

        if (slide < 0) {
            slide = currentSlide = slides.length - 1;
        }

        if (slide > slides.length - 1) {
            slide = currentSlide = 0;
        }

        slides[slide].classList.add('active');
        clientFeedbackContainer[slide].classList.add('active');

        currentActive = currentSlide;
    }

    prevArrow.addEventListener('click', () => {
        playSlide(currentSlide -= 1);
    });

    nextArrow.addEventListener('click', () => {
        playSlide(currentSlide += 1);
    });

playSlide(currentSlide);


const showSlideWithContent = (event) => {
    const clickedslider = event.target.closest('.slider')
    if (!clickedslider) {
		return
	}
    const index = Array.from(clientFeedbackSlider.children).indexOf(clickedslider)

    clientFeedbackSlider.querySelectorAll('.slider').forEach(slider => {
        slider.classList.remove('active')
    })
    clickedslider.classList.add('active')

    document.querySelectorAll('.client-feedback-container').forEach(content => {
        content.classList.remove('active')
    })
    document.querySelectorAll('.client-feedback-container')[index].classList.add('active')
}

clientFeedbackSlider.addEventListener('click', showSlideWithContent)

}