const burger = document.querySelector('[data-burger]')
const menu = document.querySelector('[data-menu-list]')
// const body = document.body
const menuItems = document.querySelectorAll('.menu__link')

burger.addEventListener('click', () => {
	// body.classList.toggle('stop-scroll')
	burger.classList.toggle('burger_active')
	menu.classList.toggle('menu__list_visible')
})

menuItems.forEach(el => {
	el.addEventListener('click', () => {
		// body.classList.remove('stop-scroll')
		burger.classList.remove('burger_active')
		menu.classList.remove('menu__list_visible')
	})
})
