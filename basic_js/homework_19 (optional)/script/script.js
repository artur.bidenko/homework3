function assessProjectDelivery(teamSpeed, backlog, deadline) {
	const workingDays = getWorkingDaysUntilDeadline(deadline);
	const totalTeamSpeed = teamSpeed.reduce((acc, curr) => acc + curr, 0);
	const totalBacklog = backlog.reduce((acc, curr) => acc + curr, 0);
	const estimatedDays = Math.ceil(totalBacklog / totalTeamSpeed);

	if (estimatedDays <= workingDays) {
		const daysBeforeDeadline = getWorkingDaysUntilDeadline(deadline, estimatedDays);
		console.log(`Усі завдання будуть успішно виконані за ${daysBeforeDeadline} днів до настання дедлайну!`);
	} else {
		const additionalHours = getAdditionalHours(estimatedDays, workingDays);
		console.log(`Команді розробників доведеться витратити додатково ${additionalHours} годин після дедлайну, щоб виконати всі завдання в беклозі`);
	}
}


function getWorkingDaysUntilDeadline(deadline, additionalDays = 0) {
	const oneDay = 24 * 60 * 60 * 1000;
	const startDate = new Date();
	let workingDays = 0;

	while (startDate <= deadline) {
		if (startDate.getDay() !== 0 && startDate.getDay() !== 6) {
		workingDays++;
		}
		startDate.setTime(startDate.getTime() + oneDay);
	}

	return workingDays - additionalDays;
}

function getAdditionalHours(estimatedDays, workingDays) {
	const workHoursPerDay = 8;
	const workingHours = workingDays * workHoursPerDay;
	const estimatedHours = estimatedDays * workHoursPerDay;
	return estimatedHours - workingHours;
}


const teamSpeed = [5, 5, 5];
const backlog = [5, 15, 5, 10, 10];
const deadline = new Date('2023-03-26');

console.log(assessProjectDelivery(teamSpeed, backlog, deadline))