import styled from "styled-components";

export const BackSpan = styled.span`
    display: inline-block;
cursor: pointer;
margin-right: 10px;
transform: rotate(180deg);

:hover {
    transform: scale(1.2) rotate(180deg);
}

svg {
    width: 32px;
    height: 25px;
}
`