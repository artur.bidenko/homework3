import { Component } from 'react'
import styled from 'styled-components'

const StyledFooter = styled.footer`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(3,37,65, 1);
    min-height: 40px;
    width: 100%;
    position: fixed;
    bottom: 0;
`

const FooterText = styled.p`
    font-size: 16px;
    font-weight: 400;
    color: #fff;
    padding: 5px;
    margin: 0;
`

export default class Footer extends Component {
    render() {
        return (
            <StyledFooter className="g-footer">
                <FooterText className="copywriting">Footer (c) 2022</FooterText>
            </StyledFooter>
        )
    }
}
