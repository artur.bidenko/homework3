/*
Теоретичні питання

	1. C помощью var, let или const, напрмер: const number;

	2. prompt выводит модальное окно с текстом и инпутом для ввода значения и кнопками да/отмена, так же можна указать необязательное начальное значение в инпут вторым аргументом. Confirm модалка с вопросом и кнопками да/отмена

	3. Это когда мы используем значения разных типов и не меняем тип целенаправленно, например: 
	const num = '3'; 
	const age = 15;
	const res = num + age - неявное преобразование в строку '315';
*/

let admin;
const myName = 'Arthur';
admin = myName;
console.log(admin);


const days = Math.floor(Math.random()*10) + 1;
console.log(days);
const seconds = days * 24 * 60 * 60;
console.log(seconds);


const userName = prompt('Введите свое имя', 'Arthur');
console.log(userName);