/*Теоретичні питання

1.
	Number
	String
	Boolean
	Null
	Object
	Undefined
	symbol
	BigInt

2. 
	== это не строгое сравние (сравнивает значения приводя их к одному типу)
	=== строгое сравнение без приведения типов 

3.
	Сложение +,
	Вычитание -,
	Умножение *,
	Деление /,
	Взятие остатка от деления %,
	Возведение в степень **,
	взятие остатка %.
	Простое сравнение ==
	Строгое стравнение ===
	не равенство !=
	строгое не равенство !==
	так же логические операторы
	&&(и) находит первое ложное значение, если все операнды true тогда возвращает последний
	||(или) находит первое истинное значение, если такого нет вернет false
	!(не) возвращает противоположное значение после преведения к булевому типу 
*/

const ageMessage = 'Вкажіть ваш вік';
const nameMessage = 'Вкажіть ваше ім\'я';

let userName = prompt(nameMessage, '');

while (userName === '' || Number(userName)) {
	userName = prompt(nameMessage, userName);
}

let userAge = prompt(ageMessage, '');
let invalidAge = userAge;
while (!Number(userAge) || !invalidAge) {
	const currentAge = prompt(ageMessage, userAge);
	if (Number(currentAge)) {
		userAge = Number(currentAge)
		invalidAge = Number(currentAge);
	} else {
		invalidAge = currentAge;
	}
}

if (userAge < 18) {
	alert('You are not allowed to visit this website');
} else if(userAge >= 18 && userAge <= 22) {
	const isConfirmed = confirm('Are you sure you want to continue?');
	userAge = (isConfirmed) ? 
		alert(`Welcome, ${userName}`) : 	
		alert('You are not allowed to visit this website!');
} else {
	alert(`Welcome, ${userName}`);
} 