var drawCircleBtn = document.getElementById('btn');
		drawCircleBtn.addEventListener('click', function() {
			var diameter = parseInt(prompt('Enter the diameter of the circle:'));
			var radius = diameter / 2;

			var container = document.createElement('div');
			container.style.width = diameter + 'px';
			container.style.height = diameter + 'px';
			// container.style.paddingTop = '500px';
			container.style.display = 'inline';

			for (var i = 0; i < 100; i++) {
				var circle = document.createElement('div');
				circle.classList.add('circle');
				circle.style.background = getRandomColor();
				circle.style.marginTop = (diameter) + 'px';
				circle.style.marginLeft = (radius) + 'px';
				circle.addEventListener('click', function() {
					this.parentNode.removeChild(this);
				});
				container.appendChild(circle);
			}

			document.body.appendChild(container);
		});

		function getRandomColor() {
			const r = Math.floor(Math.random() * 255);
			const g = Math.floor(Math.random() * 255);
			const b = Math.floor(Math.random() * 255);
		  
			return `rgb(${r}, ${g}, ${b})`;
		  }
		  