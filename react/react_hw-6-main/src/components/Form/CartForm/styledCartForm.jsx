import styled from "styled-components";

export const FlexWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
`
export const FormWrapper = styled.div`
    width: 100%;
    height: min-content;
    margin: 15px 0 40px ;
`
export const BtnWrapper = styled(FlexWrapper)`
    justify-content: center;
    margin: 15px 0; 
`
export const StyledFieldset = styled.fieldset`
    border-radius: 8px;
`
export const FormHeader = styled.h2`
    width: max-content;
    margin: 10px auto;
`
export const Price = styled.p`
    width: max-content;
    margin: 0px auto 20px;
    font-size: 1.3rem;
`