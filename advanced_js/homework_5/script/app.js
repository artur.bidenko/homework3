class Card {
	constructor(post) {
		this.post = post;
	}
	
	render() {
		const { id, title, body, user } = this.post;

		const card = document.createElement('div');
		card.className = 'card';
		card.innerHTML += `
			<span class="user">${user.name}</span>
			<span class="email">${user.email}</span>
			<div class="title">${title}</div>
			<div class="content">${body}</div>
			<a class="delete-btn" data-id="${id}">Delete</a>
		`

		const deleteBtn = card.querySelector('.delete-btn');
		deleteBtn.addEventListener('click', this.deleteCard.bind(this));

		return card;
	}

	async deleteCard(event) {
		try {
			const postId = event.target.dataset.id;
			const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, { 
				method: 'DELETE' 
			})
			if (response.ok) {
				const card = event.target.parentNode;
				card.parentNode.removeChild(card);
			}
		}
		catch (error) {
			console.error(error);
		}
	}
}

(async () => {
	try {
		const response = await fetch('https://ajax.test-danit.com/api/json/users')
		const users = await response.json()
		// console.log(users)
		async function renderPosts() {
			const posts = await (await fetch('https://ajax.test-danit.com/api/json/posts')).json()
			// console.log(posts)
			const newsFeed = document.querySelector('.news-feed');

			posts.forEach(post => {
				const user = users.find(item => item.id === post.userId);
				// console.log(user)
				if (user) {
					post.user = user;
					const card = new Card(post).render();
					newsFeed.appendChild(card);
					// console.log(newsFeed)
				}
			});
		}
		renderPosts()
	}
	catch (error) {
		console.error(error);
	}
})()